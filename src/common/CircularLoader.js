import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  root: {
    position: "absolute",
    zIndex: 10000,
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
}));

const CircularLoader = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CircularProgress style={{color: props.color}}/>
    </div>
  );
}
export default CircularLoader 