import React, { useState } from "react";
import FileUploader from "../file-uploader/FileUploader";
import firebaseService from "../../service/FirebaseService";
import ModalLoader from "../modal-loader/ModalLoader";
import { withRouter } from "react-router-dom";
import { forkJoin } from "rxjs";
import { CardContent, Typography, Card, Box, FormControl, FormHelperText, Divider, Button, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  formTitle: {
    margin: theme.spacing(3),
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  form: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    width: "88%",
  },
  fileUploaderMulti: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
  helperFileMulti: (props) => ({
    color: props.helperFileMultiColor,
    paddingLeft: theme.spacing(2),
  }),
  footer: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
}));

const ProductForm = (props) => {
  const [errorStyle, setErrorStyle] = useState({
    helperFileMultiColor: "rgba(0, 0, 0, 0.54)",
  });
  const classes = useStyles(errorStyle);
  const [selectedFiles, setSelectedFiles] = useState([]);
  const [uploadErrorMulti, setUploadErrorMulti] = useState(false);
  const [loading, setLoading] = useState(false);
  const [percent, setPercent] = useState(0);

  const changeUploaderMulti = (e) => {
    if (e) {
      setSelectedFiles(e);
      changeFileError(false, true);
    } else {
      changeFileError(true, true);
    }
  };

  const changeFileError = (isError, multi) => {
    if (isError) {
      let temp = errorStyle;
      if (multi) {
        temp.helperFileMultiColor = "#f44336";
        setUploadErrorMulti(true);
      } 
      setErrorStyle(temp);
    } else {
      let temp = errorStyle;
      if (multi) {
        temp.helperFileMultiColor = "rgba(0, 0, 0, 0.54)";
        setUploadErrorMulti(false);
      }
      setErrorStyle(temp);
    }
  };

  const postProgress = (percent) => {
    setPercent(percent);
  };

  const renderLoader = () => {
    return (
      <ModalLoader
        percent={percent}
        text={`Loading ${Math.floor(percent)} %`}
        close={!loading}
      />
    );
  };

  const submit = () => {
    if (selectedFiles.length <= 0) {
      changeFileError(true, true);
    }
    if (selectedFiles.length > 0) {
      setLoading(true);
      firebaseService.db
        .collection("banner")
        .add({
          name: new Date().toISOString().substring(0, 10),
        })
        .then((docRef) => {
          const fileName = docRef.id;
          const uploadFiles = [];
          const detailImagesName = [];
          for (let i = 0; i < selectedFiles.length; i++) {
            detailImagesName.push(`${fileName}_${i}`);
            uploadFiles.push(
              firebaseService.uploadImage(
                selectedFiles[i],
                `${fileName}_${i}`,
                postProgress
              )
            );
          }

          forkJoin(uploadFiles).subscribe(
            (urls) => {
              firebaseService.db
                .collection("banner")
                .doc(docRef.id)
                .update({
                  detailImages: detailImagesName,
                  detailImagesUrls: urls,
                })
                .then(() => {
                  setLoading(false);
                  props.history.push("/admin");
                });
            },
            (error) => {
              console.error("Error uploading file: ", error);
              setLoading(false);
              firebaseService.db
                .collection("banner")
                .doc(docRef.id)
                .delete()
                .then(() => {
                  console.log(`Created document rollback succeed`);
                });
            }
          );
        });
    }
  };
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" className={classes.formTitle}>
          Banner
        </Typography>
        <FormControl className={classes.form}>
          <Box className={classes.fileUploaderMulti}>
            <FileUploader
              id="multi2"
              onSelect={changeUploaderMulti}
              isError={uploadErrorMulti}
              multi
            />
            <FormHelperText className={classes.helperFileMulti}>
              Required image upload.
            </FormHelperText>
          </Box>
          <Divider />
        </FormControl>
        <div className={classes.footer}>
          <Button variant="contained" color="primary" onClick={submit}>
            Change
          </Button>
        </div>
        {renderLoader()}
      </CardContent>
    </Card>
  );
};
export default withRouter(ProductForm);
