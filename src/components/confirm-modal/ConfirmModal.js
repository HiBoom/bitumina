import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import { Card, CardContent, Typography, Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  action: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  card: {
    width: "300px",
  },
  title: {
    textAlign: "center",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(3),
  },
}));

const ConfirmModal = (props) => {
  const classes = useStyles();
  return (
    <Modal
      aria-labelledby="loader-modal-title"
      className={classes.modal}
      open={props.isOpen}
      onClose={props.close}
    >
      <Card className={classes.card}>
        <CardContent>
          <Typography
            variant="h5"
            color="primary"
            className={classes.title}
            id="loader-modal-title"
          >
            {props.text || "Та итгэлтэй байна уу?"}
          </Typography>
          <div className={classes.action}>
            <Button
              variant="contained"
              color="secondary"
              style={{ margin: "0px 16px" }}
              onClick={() => props.onClose(true)}
            >
              Yes
            </Button>
            <Button
              variant="contained"
              color="primary"
              style={{ margin: "0px 16px" }}
              onClick={() => props.onClose(false)}
            >
              No
            </Button>
          </div>
        </CardContent>
      </Card>
    </Modal>
  );
};
export default ConfirmModal;
