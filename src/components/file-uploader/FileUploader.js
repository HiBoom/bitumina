import React, { useState, useRef, useEffect } from "react";
import { makeStyles, Icon, Typography, Grid } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import "./style.scss";

const useStyles = makeStyles((theme) => ({
  fileSelect: (props) => ({
    padding: theme.spacing(1),
    border: 1,
    borderColor: props.borderColor,
    borderRadius: 3,
    borderStyle: "solid",
  }),
  fileEmpty: {
    flexDirection: "column",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    fontSize: 50,
  },
  control: {
    display: "flex",
    justifyContent: "spacing-between",
    alignItems: "center",
    padding: theme.spacing(2),
    position: "relative",
    width: "100%",
    backgroundColor: "rgba(0,0,0,0.3)",
  },
  multiImages: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  controlMulti: {
    display: "flex",
    justifyContent: "spacing-between",
    alignItems: "center",
    padding: "4px",
    width: "100%",
    backgroundColor: "rgba(0,0,0,0.3)",
  },
  controlTextMulti: {
    width: "70%",
    fontSize: "10px",
  },
  imageMulti: {
    width: "100%",
    height: "15vh"
  },
  controlText: {
    width: "80%",
  },
  image: {
    width: "100%",
  },
}));
const formatFileSize = (bytes) => {
  if (bytes === 0) return "0 Bytes";
  var k = 1000,
    sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
    i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat(bytes / Math.pow(k, i)).toFixed(1) + " " + sizes[i];
};

const renderFile = (
  fileInput,
  classes,
  isUpload,
  fileUrl,
  uploadFileDetail,
  clearParent,
  isMulti
) => {
  const clear = () => {
    clearParent();
  };
  if (isUpload) {
    if (!isMulti) {
      return (
        <div>
          <div className={classes.control}>
            <Grid item xs={12}>
              <Typography noWrap className={classes.controlText}>
                {uploadFileDetail}
              </Typography>
            </Grid>
            <IconButton size="small" className="controlButton" onClick={clear}>
              <CloseIcon />
            </IconButton>
          </div>
          <img className={classes.image} alt="zurag" src={fileUrl} />
        </div>
      );
    } else {
      return (
        <div className={classes.multiImages}>
          {fileUrl.map((url, i) => {
            return (
              <div key={`img_${i}`} style={{width: "24%", padding: "2px"}}>
                <div className={classes.controlMulti}>
                  <Grid item xs={12}>
                    <Typography noWrap className={classes.controlTextMulti}>
                      {uploadFileDetail[i]}
                    </Typography>
                  </Grid>
                  <IconButton
                    size="small"
                    className="controlButtonMulti"
                    onClick={clear}
                  >
                    <CloseIcon />
                  </IconButton>
                </div>
                <img className={classes.imageMulti} alt="zurag" src={url} />
              </div>
            );
          })}
        </div>
      );
    }
  }
  return (
    <div
      className={`MuiFormHelperText-root ${classes.fileEmpty}`}
      onClick={() => fileInput.current.click()}
    >
      <Icon className={classes.icon}>open_in_browser</Icon>
      Файл байршуулна уу!
    </div>
  );
};
const FileUploader = (props) => {
  const [errorStyle, setErrorStyle] = useState({
    borderColor: "rgba(0, 0, 0, 0.25)",
  });
  const classes = useStyles(errorStyle);
  const [isUpload, setIsUpload] = useState(false);
  const [uploadFileDetail, setUploadFileDetail] = useState("");
  const [uploadFileDetailMulti, setUploadFileDetailMulti] = useState("");
  const [fileUrl, setFileUrl] = useState("");
  const [fileUrls, setFileUrls] = useState([]);
  const [isError, setIsError] = useState(false);

  const fileInput = useRef(null);

  const fileSelectedHandler = (event) => {
    if (event.target.files.length > 0) {
      setIsUpload(true);

      if (props.multi) {
        const urls = [];
        const detail = [];
        for (let i = 0; i < event.target.files.length; i++) {
          urls.push(URL.createObjectURL(event.target.files[i]));
          detail.push(
            `${event.target.files[i].name} ( ${formatFileSize(
              event.target.files[i].size
            )} )`
          );
        }
        setUploadFileDetailMulti(detail);
        setFileUrls(urls);
      } else {
        setUploadFileDetail(
          `${event.target.files[0].name} ( ${formatFileSize(
            event.target.files[0].size
          )} )`
        );
        setFileUrl(URL.createObjectURL(event.target.files[0]));
      }

      if (props.multi)
        props.onSelect(event.target.files);
      else props.onSelect(event.target.files[0]);
    } else {
      setIsUpload(false);
      if(props.multi) {
        setUploadFileDetailMulti([]);
        setFileUrls([]);
      } else {
        setUploadFileDetail("");
        setFileUrl("");
      }
    }
  };

  const clear = () => {
    if (props.multi) {
      fileInput.current.value = null;
      props.onSelect(null);
      setIsUpload(false);
      setFileUrls([]);
      setUploadFileDetailMulti([]);
    } else {
      fileInput.current.value = null;
      props.onSelect(null);
      setIsUpload(false);
      setFileUrl("");
      setUploadFileDetail("");
    }
  };

  useEffect(() => {
    if (isError !== props.isError) {
      if (props.isError) {
        let temp = errorStyle;
        temp.borderColor = "#f44336";
        setErrorStyle(temp);
        setIsError(true);
      } else {
        let temp = errorStyle;
        temp.borderColor = "rgba(0, 0, 0, 0.25)";
        setErrorStyle(temp);
        setIsError(false);
      }
    }
  }, [props.isError, isError, errorStyle]);

  return (
    <div>
      <input
        type="file"
        name="file"
        hidden="hidden"
        accept="image/png, image/jpeg"
        onChange={fileSelectedHandler}
        multiple={props.multi}
        ref={fileInput}
      />
      <div className={`fileInput ${classes.fileSelect}`}>
        {renderFile(
          fileInput,
          classes,
          isUpload,
          props.multi ? fileUrls : fileUrl,
          props.multi ? uploadFileDetailMulti : uploadFileDetail,
          clear,
          props.multi
        )}
      </div>
    </div>
  );
};
export default FileUploader;
