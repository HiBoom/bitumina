import React, { useContext } from "react";
import ThemeContext from "../../context/theme-context/ThemeContext";
import LanguageContext from "../../context/lang-context/LanguageContext";
import { Link } from "react-router-dom";
import "./header.scss";

const Header = () => {
  const themeContext = useContext(ThemeContext);
  const langContext = useContext(LanguageContext);
  const langText = langContext.language.data
  return (
    <div className="navbar">
      Navbar
      <Link className="nav-item-link" to="/">
        <button className="nav-item">
          {langText.homePageText}
        </button>
      </Link>
      <Link className="nav-item-link" to="/admin">
        <button className="nav-item">
          {langText.adminPageText}
        </button>
      </Link>
      <button
        className="nav-item"
        onClick={() => {
          themeContext.setTheme(themeContext.theme.type);
        }}
      >
        {langText.changeThemeText}
      </button>
      <button
        className="nav-item"
        onClick={() => {
          langContext.setLanguage(langContext.language.options);
        }}
      >
        {langText.changeLangText}
      </button>
    </div>
  );
};
export default Header;
