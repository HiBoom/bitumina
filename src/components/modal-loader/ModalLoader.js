import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { Card, CardContent, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  loader: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  card: {
    width: "300px",
    height: "120px"
  },
  title: {
    textAlign: 'center',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(3),
  }
}));

const ModalLoader = (props) => {
  const classes = useStyles();
  const rootRef = React.useRef(null);
  const [completed, setCompleted] = React.useState(0);

  useEffect(() => {
    setCompleted(props.percent);
  }, [props.percent, setCompleted]);

  return (
    <div className={classes.root} ref={rootRef}>
      <Modal
        aria-labelledby="loader-modal-title"
        className={classes.modal}
        disablePortal
        disableEnforceFocus
        disableAutoFocus
        disableBackdropClick
        open={!props.close}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={true}>
          <Card className={classes.card}>
            <CardContent>
              <Typography variant="h5" color="primary" className={classes.title} id="loader-modal-title">{props.text}</Typography>
              <div className={classes.loader}>
                <LinearProgress variant="determinate" value={completed} />
              </div>
            </CardContent>
          </Card>
        </Fade>
      </Modal>
    </div>
  );
};
export default ModalLoader;
