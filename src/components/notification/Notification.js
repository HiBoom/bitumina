import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import { Card, CardContent, Typography, Button } from "@material-ui/core";
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import amber from '@material-ui/core/colors/amber';
const successColor = green[600];
const errorColor = red[600];
const warningColor = amber[500];

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  action: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  cardContent: {
    textAlign: "center"
  },
  icon: {
    height: "40px",
    width: "40px"
  },
  card: {
    width: "300px",
  },
  title: {
    textAlign: "center",
    marginTop: 0,
    marginBottom: theme.spacing(1),
  },
  detail: {
    marginTop: 0,
    marginBottom: theme.spacing(2),
  }
}));


const Notification = (props) => {
  const classes = useStyles();
  
  const renderIcon = (status) => {
    if(status === "success")
      return <CheckCircleOutlineIcon style={{ fontSize: 45, color: successColor }}/>
    if(status === "error")
      return <HighlightOffIcon style={{ fontSize: 45, color: errorColor }}/>
    return <ErrorOutlineIcon style={{ fontSize: 45, color: warningColor }}/>
  }

  const renderButton = (status, close) => {
    let selectedColor = successColor; 
    let tintColor = "#FFFFFF";
    if(status === "success") {
      selectedColor = successColor;
      tintColor = "#FFFFFF";
    }
    if(status === "error") {
      selectedColor = errorColor;
      tintColor = "#FFFFFF";
    }
    if(status === "warning") {
      selectedColor = warningColor;
      tintColor = "#000000";
    }

    return (
      <Button
        variant="contained"
        style={{ margin: "0px 16px", backgroundColor: selectedColor, color: tintColor }}
        onClick={() => close()}
      >
        Close
      </Button>
    )
  }

  const renderText = (status, text, detailText) => {
    let defaultText = "";
    let _detailText = "";
    if(status === "success") {
      defaultText = "Амжилттай"
    }
    if(status === "error") {
      defaultText = "Алдаа гарлаа"
    }
    if(status === "warning") {
      defaultText = "Анхааруулга"
    }
    if(text)
      defaultText = text
    if(detailText)
      _detailText = detailText;

    return (
      <div>
        <Typography
          variant="h5"
          className={classes.title}
          id="loader-modal-title"
        >
          {defaultText}
        </Typography>
        <Typography
          variant="body1"
          className={classes.detail}
          >
          {_detailText}
        </Typography>
      </div>
    )
  }

  return (
    <Modal
      aria-labelledby="loader-modal-title"
      className={classes.modal}
      open={props.isOpen}
      onClose={props.close}
    >
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          {renderIcon(props.status)}
          {renderText(props.status, props.title, props.detail)}
          <div className={classes.action}>
            {renderButton(props.status, props.close)}
          </div>
        </CardContent>
      </Card>
    </Modal>
  );
};
export default Notification;
