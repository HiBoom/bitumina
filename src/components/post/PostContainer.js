import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";
import PostList from "./PostList";
import firebaseService from "../../service/FirebaseService";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));

const PostContainer = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [postList, setPostList] = useState([]);

  useEffect(() => {
    firebaseService.db
      .collection("posts")
      .get()
      .then((snapshot) => {
        const datas = [];
        snapshot.forEach((doc) => {
          datas.push({
            id: doc.id,
            ...doc.data(),
          });
        });
        
        setPostList(datas);
        setLoading(false);
      });
  }, []);

  return (
    <div className={classes.root}>
      <PostList postsData={postList} loading={loading}/>
    </div>
  );
};
export default PostContainer;
