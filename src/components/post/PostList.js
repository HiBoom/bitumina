import React from "react";
import Grid from "@material-ui/core/Grid";
import Post from "./Post";

const PostList = (props) => {
  const { postsData, loading } = props;
  return (
    <Grid container spacing={3}>
      {
        (loading ? Array.from(new Array(3)) : postsData).map((post, index) => (
          <Grid item xs={12} sm={12} md={4} key={`post_${index}`}>
            <Post post={post}/>
          </Grid>
        ))
      }
    </Grid>
  );
};
export default PostList;
