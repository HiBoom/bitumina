import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";
import ProductList from "./ProductList";
import firebaseService from "../../service/FirebaseService";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));

const ProductContainer = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [productList, setProductList] = useState([]);

  useEffect(() => {
    firebaseService.db
      .collection("products")
      .get()
      .then((snapshot) => {
        const datas = [];
        snapshot.forEach((doc) => {
          datas.push({
            id: doc.id,
            ...doc.data(),
          });
        });
        
        setProductList(datas);
        setLoading(false);
      });
  }, []);

  return (
    <div className={classes.root}>
      <ProductList productsData={productList} loading={loading}/>
    </div>
  );
};
export default ProductContainer;
