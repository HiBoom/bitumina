import React from "react";
import Grid from "@material-ui/core/Grid";
import Product from "./Product";

const ProductList = (props) => {
  const { productsData, loading } = props;
  
  return (
    <Grid container spacing={3}>
      {
        (loading ? Array.from(new Array(3)) : productsData).map((product, index) => (
          <Grid item xs={12} sm={12} md={4} key={`product_${index}`}>
            <Product product={product}/>
          </Grid>
        ))
      }
    </Grid>
  );
};
export default ProductList;
