import React from "react";
import StyleButton from "./StyledButton";

const BLOCK_TYPES = [
  { label: "H1", style: "header-one", icon: 'home' },
  { label: "H2", style: "header-two", icon: 'home' },
  { label: "H3", style: "header-three", icon: 'home' },
  { label: "H4", style: "header-four", icon: 'home' },
  { label: "H5", style: "header-five", icon: 'home' },
  { label: "H6", style: "header-six", icon: 'home' },
  { label: "Blockquote", style: "blockquote", icon: 'format_quote' },
  { label: "UL", style: "unordered-list-item", icon: 'format_list_bullet' },
  { label: "OL", style: "ordered-list-item", icon: 'format_list_number' }
];

const BlockStyleControls = (props) => {
  const { editorState } = props;
  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  return (
    <div className="RichEditor-controls">
      {BLOCK_TYPES.map((type) => (
        <StyleButton
          key={type.label}
          active={type.style === blockType}
          icon={type.icon}
          onToggle={props.onToggle}
          style={type.style}
        />
      ))}
    </div>
  );
};
export default BlockStyleControls;
