import React from "react";
import StyleButton from "./StyledButton";

const INLINE_STYLES = [
  { label: "Bold", style: "BOLD", icon: "format_bold" },
  { label: "Italic", style: "ITALIC", icon: "format_italic" },
  { label: "Underline", style: "UNDERLINE", icon: "format_underline" },
  { label: "Monospace", style: "CODE", icon: "code" },
];

const InlineStyleControls = (props) => {
  const currentStyle = props.editorState.getCurrentInlineStyle();
  return (
    <div className="RichEditor-controls">
      {INLINE_STYLES.map((type) => (
        <StyleButton
          key={type.label}
          active={currentStyle.has(type.style)}
          icon={type.icon}
          onToggle={props.onToggle}
          style={type.style}
        />
      ))}
    </div>
  );
};
export default InlineStyleControls;
