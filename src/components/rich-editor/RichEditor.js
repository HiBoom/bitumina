import React, { useState, useRef, useEffect } from "react";
import { Editor, EditorState, RichUtils } from "draft-js";
import InlineStyleControls from "./InlineStyledControls";
import { makeStyles, Divider } from "@material-ui/core";
import { stateToHTML } from "draft-js-export-html";
import "./style.scss";

const useStyles = makeStyles((theme) => ({
  editorRoot: (props) => ({
    border: 1,
    borderColor: props.borderColor,
    borderStyle: "solid",
    borderRadius: 3,
    padding: 10,
  }),
  editorDivider: {
    marginBottom: theme.spacing(1),
  },
}));

const RichEditor = (props) => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [isError, setIsError] = useState(false);
  const editor = useRef(null);
  const [errorStyle, setErrorStyle] = useState({
    borderColor: "rgba(0, 0, 0, 0.25)",
  });

  const classes = useStyles(errorStyle);

  const focus = () => {
    editor.current.focus();
  };

  const onChangeEditor = (editorState) => {
    setEditorState(editorState);
    props.onChange(stateToHTML(editorState.getCurrentContent()));
  };

  const toggleInlineStyle = (inlineStyle) => {
    onChangeEditor(RichUtils.toggleInlineStyle(editorState, inlineStyle));
  };

  useEffect(() => {
    if (isError !== props.isError) {
      if (props.isError) {
        let temp = errorStyle;
        temp.borderColor = "#f44336";
        setErrorStyle(temp);
        setIsError(true);
      } else {
        let temp = errorStyle;
        temp.borderColor = "rgba(0, 0, 0, 0.25)";
        setErrorStyle(temp);
        setIsError(false);
      }
    }
  }, [props.isError, isError, errorStyle]);

  return (
    <div className={`editorBorder ${classes.editorRoot}`}>
      <InlineStyleControls
        editorState={editorState}
        onToggle={toggleInlineStyle}
      />
      <Divider className={classes.editorDivider} />
      <div onClick={focus}>
        <Editor
          editorState={editorState}
          onChange={onChangeEditor}
          ref={editor}
        />
      </div>
      <div hidden={true}>{isError}</div>
    </div>
  );
};
export default RichEditor;
