import React from 'react';
import Icon from '@material-ui/core/Icon';

const StyleButton = (props) => {
    
    const onToggle = (e) => {
        e.preventDefault();
        props.onToggle(props.style);
    };

    let className = 'MuiFormHelperText-root';
    if (props.active) {
        className = '';
    }

    return (
        <span className={className} onMouseDown={onToggle}>
            <Icon>{props.icon}</Icon>
        </span>
    );
}
export default StyleButton;