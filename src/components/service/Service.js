import React, { useState } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Skeleton from "@material-ui/lab/Skeleton";
import { makeStyles, Box } from "@material-ui/core";
import "./style.scss";
import ConfirmModal from "../confirm-modal/ConfirmModal";
import firebaseService from "../../service/FirebaseService";
import { withRouter } from "react-router-dom";
import ModalLoader from "../modal-loader/ModalLoader";
import { forkJoin } from "rxjs";
import functionsService from "../../service/FunctionsService";

const useStyles = makeStyles((theme) => ({
  image: {
    width: "100%",
  },
  productName: {
    fontSize: "16px",
    fontWeight: "500",
  },
  serviceHtmlBody: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    fontSize: "14px",
    height: "75px",
  },
  date: {
    fontSize: "12px",
  },
  buttons: {
    display: "flex",
  },
  action: {
    position: "relative",
    backgroundColor: "#fff",
  },
  button: {
    marginRight: theme.spacing(2),
  },
}));

const Service = (props) => {
  const classes = useStyles();
  const [isOpenConfirm, setIsOpenConfirm] = useState(false);
  const [loading, setLoading] = useState(false);
  const [percent, setPercent] = useState(0);
  const { service } = props;

  const callConfirm = () => {
    setIsOpenConfirm(true);
  };

  const closeModal = (result) => {
    if (result) {
      setLoading(true);
      const { id, detailImages } = props.service;
      const deleteUrl = [firebaseService.storage.ref(`images/${id}`).delete()];

      for(let i = 0; i < detailImages.length; i++) {
        deleteUrl.push(firebaseService.storage.ref(`images/${detailImages[i]}`).delete())
      }

      forkJoin(deleteUrl).subscribe(() => {
          setPercent(50);
          firebaseService.db
            .collection("services")
            .doc(id)
            .delete()
            .then(() => {
              setPercent(100);
              setTimeout(() => {
                setLoading(false);
                props.history.push("/admin");
              }, 1000);
            });
        });
    }
    setIsOpenConfirm(false);
  };

  const renderLoader = () => {
    return (
      <ModalLoader
        percent={percent}
        text={`Loading ${Math.floor(percent)} %`}
        close={!loading}
      />
    );
  };

  return (
    <Card className={classes.card}>
      {renderLoader()}
      <CardContent>
        {service ? (
          <Box>
            <Typography gutterBottom>
              <b>{service.serviceName}</b>
            </Typography>
            <Typography
              color="textSecondary"
              className={classes.date}
              gutterBottom
            >
              {service.createdDate}
            </Typography>
          </Box>
        ) : (
          <Box>
            <Skeleton variant="text" animation="wave" width="60%" height={20} />
            <Skeleton variant="text" animation="wave" width="60%" height={30} />
            <Skeleton variant="text" animation="wave" width="30%" />
          </Box>
        )}

        {service ? (
          <div
            className="cardImage"
            style={{ backgroundImage: `url(${service.imageUrl})` }}
          ></div>
        ) : (
          // <img alt={service.id} src={service.imageUrl} className={classes.image} />
          <Skeleton variant="rect" height={200} width="100%" />
        )}
        {service ? (
          <Typography
            variant="body2"
            className={classes.serviceHtmlBody}
            paragraph={true}
            // dangerouslySetInnerHTML={{ __html: service.serviceBodyHtml }}
          >
            {functionsService.textLimit(service.serviceDescriptionText, 200)}
          </Typography>
        ) : (
          <Box>
            <Skeleton variant="text" animation="wave" width="100%" />
            <Skeleton variant="text" animation="wave" width="100%" />
            <Skeleton variant="text" animation="wave" width="50%" />
          </Box>
        )}
      </CardContent>
      <CardActions className={classes.action}>
        {service ? (
          <Box>
            <Button variant="contained" className={classes.button}>
              More
            </Button>
            <Button
              variant="contained"
              className={classes.button}
              color="secondary"
              onClick={callConfirm}
            >
              Delete
            </Button>
          </Box>
        ) : (
          <Box className={classes.buttons}>
            <Skeleton
              variant="rect"
              className={classes.button}
              animation="wave"
              height={35}
              width={70}
            />
            <Skeleton
              variant="rect"
              className={classes.button}
              animation="wave"
              height={35}
              width={70}
            />
          </Box>
        )}
        <ConfirmModal
          isOpen={isOpenConfirm}
          onClose={closeModal}
          close={() => setIsOpenConfirm(false)}
          text="Та үйлчилгээг устгах гэж байна!"
        />
      </CardActions>
    </Card>
  );
};
export default withRouter(Service);
