import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core";
import ServiceList from "./ServiceList";
import firebaseService from "../../service/FirebaseService";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
}));

const PostContainer = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(true);
  const [serviceList, setServiceList] = useState([]);

  useEffect(() => {
    firebaseService.db
      .collection("services")
      .get()
      .then((snapshot) => {
        const datas = [];
        snapshot.forEach((doc) => {
          datas.push({
            id: doc.id,
            ...doc.data(),
          });
        });
        
        setServiceList(datas);
        setLoading(false);
      });
  }, []);

  return (
    <div className={classes.root}>
      <ServiceList servicesData={serviceList} loading={loading}/>
    </div>
  );
};
export default PostContainer;
