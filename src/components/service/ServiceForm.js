import React, { useState } from "react";
import {
  FormControl,
  FormHelperText,
  Card,
  CardContent,
  Typography,
  makeStyles,
  Box,
  Divider,
  Button,
  FormGroup,
  TextField,
} from "@material-ui/core";
import FileUploader from "../file-uploader/FileUploader";
import firebaseService from "../../service/FirebaseService";
import ModalLoader from "../modal-loader/ModalLoader";
import { withRouter } from "react-router-dom";
import { forkJoin } from "rxjs";

const useStyles = makeStyles((theme) => ({
  formTitle: {
    margin: theme.spacing(3),
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  form: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    width: "88%",
  },
  editor: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
  fileUploader: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
  helperFile: (props) => ({
    color: props.helperFileColor,
    paddingLeft: theme.spacing(2),
  }),
  fileUploaderMulti: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
  helperFileMulti: (props) => ({
    color: props.helperFileMultiColor,
    paddingLeft: theme.spacing(2),
  }),
  helperEditor: (props) => ({
    color: props.helperEditorColor,
    paddingLeft: theme.spacing(2),
  }),
  input: {
    marginBottom: theme.spacing(1),
  },
  footer: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
}));

const ServiceForm = (props) => {
  const [errorStyle, setErrorStyle] = useState({
    helperFileColor: "rgba(0, 0, 0, 0.54)",
    helperFileMultiColor: "rgba(0, 0, 0, 0.54)",
  });
  const classes = useStyles(errorStyle);
  const [serviceName, setServiceName] = useState("");
  const [serviceNameError, setServiceNameError] = useState(false);
  // const [productName, setProductName] = useState("");
  // const [productNameError, setProductNameError] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const [selectedFiles, setSelectedFiles] = useState([]);
  const [editorContent, setEditorContent] = useState("");
  const [editorError, setEditorError] = useState(false);
  const [uploadError, setUploadError] = useState(false);
  const [uploadErrorMulti, setUploadErrorMulti] = useState(false);
  const [loading, setLoading] = useState(false);
  const [percent, setPercent] = useState(0);
  const [serviceNameEn, setServiceNameEn] = useState("");
  // const [productNameEn, setProductNameEn] = useState("");
  const [editorContentEn, setEditorContentEn] = useState("");

  const changeInput = (e) => {
    switch (e.target.name) {
      case "serviceName":
        setServiceNameError(false);
        setServiceName(e.target.value);
        break;
      case "bodyText":
        setEditorError(false);
        setEditorContent(e.target.value);
        break;
      case "serviceNameEn":
        setServiceNameEn(e.target.value);
        break;
      case "bodyTextEn":
        setEditorContentEn(e.target.value);
        break;
      default:
        break;
    }
  };

  const changeUploader = (e) => {
    if (e) {
      setSelectedFile(e);
      changeFileError(false, false);
    } else {
      changeFileError(true, false);
    }
  };

  const changeUploaderMulti = (e) => {
    if (e) {
      setSelectedFiles(e);
      changeFileError(false, true);
    } else {
      changeFileError(true, true);
    }
  };

  const changeFileError = (isError, multi) => {
    if (isError) {
      
      let temp = errorStyle;
      if(multi) {
        temp.helperFileMultiColor = "#f44336";
        setUploadErrorMulti(true)
      }
      else {
        temp.helperFileColor = "#f44336";
        setUploadError(true);
      }
      setErrorStyle(temp);
    } else {
      let temp = errorStyle;
      if(multi) {
        temp.helperFileMultiColor = "rgba(0, 0, 0, 0.54)";
        setUploadErrorMulti(false);
      } else {
        temp.helperFileColor = "rgba(0, 0, 0, 0.54)";
        setUploadError(false);
      }
      setErrorStyle(temp);
    }
  };

  const serviceProgress = (percent) => {
    setPercent(percent)
  };

  const renderLoader = () => {
    return <ModalLoader percent={percent} text={`Loading ${Math.floor(percent)} %`} close={!loading}/>
  };

  const submit = () => {
    if (!serviceName) setServiceNameError(true);
    if (!editorContent) setEditorError(true);
    if (!selectedFile) {
      changeFileError(true, false);
    }
    if (selectedFiles.length <= 0) {
      changeFileError(true, true);
    }
    if (serviceName && selectedFile && selectedFiles.length > 0 && editorContent) {
      setLoading(true);

      firebaseService.db
        .collection("services")
        .add({
          serviceName: serviceName,
          serviceDescriptionText: editorContent,
          serviceNameEn: serviceNameEn,
          serviceDescriptionTextEn: editorContentEn,
          createdDate: new Date().toISOString(),
          createdBy: firebaseService.auth.currentUser.email
        })
        .then((docRef) => {
          const fileName = docRef.id;
          const uploadFiles = [
            firebaseService.uploadImage(selectedFile, fileName, serviceProgress)
          ];
          const detailImagesName = [];
          for(let i = 0; i < selectedFiles.length; i++) {
            detailImagesName.push(`${fileName}_${i}`);
            uploadFiles.push(firebaseService.uploadImage(selectedFiles[i], `${fileName}_${i}`, serviceProgress));
          }

          forkJoin(uploadFiles).subscribe((urls) => {
              firebaseService.db
                .collection("services")
                .doc(docRef.id)
                .update({ imageUrl: urls[0], detailImages: detailImagesName, detailImagesUrls: urls.slice(1, urls.length) })
                .then(() => {
                  console.log("Service created");
                  setLoading(false);
                  props.history.push("/admin")
                });
            }, error => {
              console.error("Error uploading file: ", error);
              setLoading(false);
              firebaseService.db
                .collection("services")
                .doc(docRef.id)
                .delete()
                .then(() => {
                  console.log(`Created document rollback succeed`);
                });
            })
        })
        .catch((error) => {
          console.error("Error creating service: ", error);
          setLoading(false);
        });
    }
  };
  return (
    <Card>
      <CardContent>
        <Typography variant="h5" className={classes.formTitle}>
          Create service
        </Typography>
        <FormControl className={classes.form}>
          <FormGroup className={classes.input}>
            <TextField
              error={serviceNameError}
              id="serviceNameErrorText"
              name="serviceName"
              label="Service name"
              value={serviceName}
              onChange={(e) => changeInput(e)}
              helperText="Required service name."
              variant="outlined"
            />
          </FormGroup>
          <FormGroup className={classes.input}>
            <TextField
              id="serviceNameEn"
              name="serviceNameEn"
              label="Service name english"
              value={serviceNameEn}
              onChange={(e) => changeInput(e)}
              variant="outlined"
            />
          </FormGroup>
          <Box className={classes.fileUploader}>
            <FileUploader id="single1" onSelect={changeUploader} isError={uploadError} />
            <FormHelperText className={classes.helperFile}>
              Required image upload.
            </FormHelperText>
          </Box>
          <Box className={classes.fileUploaderMulti}>
            <FileUploader id="multi2" onSelect={changeUploaderMulti} isError={uploadErrorMulti} multi/>
            <FormHelperText className={classes.helperFileMulti}>
              Required image upload.
            </FormHelperText>
          </Box>
          <FormGroup className={classes.input}>
            <TextField
              error={editorError}
              id="bodyText"
              name="bodyText"
              label="Service text"
              multiline={true}
              rows={4}
              value={editorContent}
              onChange={(e) => changeInput(e)}
              helperText="Required service text."
              variant="outlined"
            />
          </FormGroup>
          <FormGroup className={classes.input}>
            <TextField
              id="bodyTextEn"
              name="bodyTextEn"
              label="Service text english"
              multiline={true}
              rows={4}
              value={editorContentEn}
              onChange={(e) => changeInput(e)}
              variant="outlined"
            />
          </FormGroup>
          <Divider />
        </FormControl>
        <div className={classes.footer}>
          <Button variant="contained" color="primary" onClick={submit}>
            Add service 
          </Button>
        </div>
        {renderLoader()}
      </CardContent>
    </Card>
  );
};
export default withRouter(ServiceForm);
