import React from "react";
import Grid from "@material-ui/core/Grid";
import Service from "./Service";

const ServiceList = (props) => {
  const { servicesData, loading } = props;
  return (
    <Grid container spacing={3}>
      {
        (loading ? Array.from(new Array(3)) : servicesData).map((service, index) => (
          <Grid item xs={12} sm={12} md={4} key={`service_${index}`}>
            <Service service={service}/>
          </Grid>
        ))
      }
    </Grid>
  );
};
export default ServiceList;
