import React from "react";
import { makeStyles, Link } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PostAdd from "@material-ui/icons/PostAdd";
import ViewList from "@material-ui/icons/ViewList";
import PersonAdd from "@material-ui/icons/PersonAdd";
import BurstMode from "@material-ui/icons/BurstMode";
import AddBox from "@material-ui/icons/AddBox";
import ContactMail from "@material-ui/icons/ContactMail";

const useStyles = makeStyles((theme) => ({
  drawerContainer: {
    overflow: "auto",
  },
}));

const Sidebar = () => {
  const classes = useStyles();
  return (
    <div className={classes.drawerContainer}>
      <List>
        <Link href="/admin/createPost" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <PostAdd />
            </ListItemIcon>
            <ListItemText primary="Create post" />
          </ListItem>
        </Link>
        <Link href="/admin/postList" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <ViewList />
            </ListItemIcon>
            <ListItemText primary="Post list" />
          </ListItem>
        </Link>
        <Link href="/admin/addProduct" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <AddBox />
            </ListItemIcon>
            <ListItemText primary="Add product" />
          </ListItem>
        </Link>
        <Link href="/admin/productList" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <ViewList />
            </ListItemIcon>
            <ListItemText primary="Product list" />
          </ListItem>
        </Link>
        <Link href="/admin/createService" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <AddBox />
            </ListItemIcon>
            <ListItemText primary="Add service" />
          </ListItem>
        </Link>
        <Link href="/admin/serviceList" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <ViewList />
            </ListItemIcon>
            <ListItemText primary="Service list" />
          </ListItem>
        </Link>
        <Link href="/admin/contact" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <ContactMail />
            </ListItemIcon>
            <ListItemText primary="Contact info" />
          </ListItem>
        </Link>
        <Link href="/admin/addUser" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <PersonAdd />
            </ListItemIcon>
            <ListItemText primary="Add user" />
          </ListItem>
        </Link>
        <Link href="/admin/bannerForm" color="inherit">
          <ListItem button>
            <ListItemIcon>
              <BurstMode />
            </ListItemIcon>
            <ListItemText primary="Change banner" />
          </ListItem>
        </Link>
      </List>
    </div>
  );
};
export default Sidebar;
