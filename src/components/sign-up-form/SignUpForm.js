import React, { useState } from "react";
import { makeStyles, Card, Typography, FormGroup, TextField, CardContent, FormControl, Button } from "@material-ui/core";
import firebaseService from "../../service/FirebaseService";
import Notification from "../notification/Notification";

const useStyles = makeStyles((theme) => ({
  formTitle: {
    margin: theme.spacing(3),
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  form: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    width: "88%",
  },
  input: {
    marginBottom: theme.spacing(1),
  },
  footer: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
}));

const SignUpForm = (props) => {

  const [email, setEmail] = useState("");
  const [password, setPassord] = useState("");
  const [emailError, setEmailError] = useState(false);
  const [notifIsOpen, setNotifIsOpen] = useState(false);
  const [notifStatus, setNotifStatus] = useState("success");
  const [notifDetail, setNotifDetail] = useState("");
  const classes = useStyles();

  const changeInput = (e) => {
    setEmailError(false);
    setEmail(e.target.value);
  };

  const submit = () => {
    if (!email) setEmailError(true);
    else {
      firebaseService.signUpEmail(email, password).then(userCredential => {
        setNotifStatus("success");
        firebaseService.auth.currentUser.sendEmailVerification().then(() => {
          setNotifDetail("Хэрэглэгч амжиллтай бүртгэгдлээ. Цахим шуудангаар баталгаажуулах холбоос илгээлээ.")
          setNotifIsOpen(true);
        })
      }).catch((error) => {
        const { code, message } = error;
        setNotifStatus("error");
        setNotifDetail(`${code}: ${message}`);
        setNotifIsOpen(true);
      })
    }
  };

  const notifClose = () => {
    setNotifIsOpen(false);
    props.history.push("/admin");
  }

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" className={classes.formTitle}>
          Add user
        </Typography>
        <FormControl className={classes.form}>
          <FormGroup className={classes.input}>
            <TextField
              error={emailError}
              id="emailError"
              name="email"
              label="Email"
              value={email}
              onChange={(e) => changeInput(e)}
              helperText="Required email."
              variant="outlined"
            />
          </FormGroup>
          <FormGroup className={classes.input}>
            <TextField
              id="password"
              name="password"
              label="Password"
              type="password"
              value={password}
              onChange={(e) => setPassord(e.target.value)}
              helperText={`Default password is ${firebaseService.DEFAULT_PASSWORD}.`}
              variant="outlined"
            />
          </FormGroup>
        </FormControl>
        <Notification isOpen={notifIsOpen} close={notifClose} status={notifStatus} detail={notifDetail}/>
        <div className={classes.footer}>
          <Button variant="contained" color="primary" onClick={submit}>
            Sign Up
          </Button>
        </div>
      </CardContent>
    </Card>
  );
}
export default SignUpForm;