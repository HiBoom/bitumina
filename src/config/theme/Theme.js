const Themes = {
  light: {
    type: 'light',
    foreground: "#000000",
    background: "#eeeeee",
    secondary: "#00000099"
  },
  dark: {
    type: 'dark',
    foreground: "#ffffff",
    background: "#222222",
    secondary: "#eeeeee99"
  }
};
export default Themes;
