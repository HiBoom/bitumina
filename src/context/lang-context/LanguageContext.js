import React from "react";
import Languages from "../../config/lang/Language";

const LanguageContext = React.createContext({
  language: Languages.mn,
  setLanguage: () => {},
});
export default LanguageContext;