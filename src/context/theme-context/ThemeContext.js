import React from "react";
import Themes from "../../config/theme/Theme";

const ThemeContext = React.createContext({
  theme: Themes.light,
  setTheme: () => {},
});
export default ThemeContext;