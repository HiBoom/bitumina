import React, { useState } from "react";
import Themes from "../../config/theme/Theme";
import ThemeContext from "./ThemeContext";

const ThemeContextProvider = (props) => {
  const setTheme = (type) => {
    setState({ ...state, theme: type === 'light' ? Themes.dark : Themes.light});
  }

  const initState = {
    theme: Themes.light,
    setTheme: setTheme,
  };

  const [state, setState] = useState(initState);

  return (
    <ThemeContext.Provider value={state}>
      {props.children}
    </ThemeContext.Provider>
  );
};
export default ThemeContextProvider;
