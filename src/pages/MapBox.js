import React, { useState, useEffect } from 'react';
import ReactMapboxGl, {Marker} from 'react-mapbox-gl';

export default function MapBox(props) {

  useEffect(() => {
    setSelectedCenterCoordinate(centerCoordinates[props.object]);
  }, [props.object]);

  const Map = ReactMapboxGl({  
    accessToken: "pk.eyJ1IjoiYml0dW1pbmEiLCJhIjoiY2thcTlpMmluMDkxczJ4cGxxaDlvNnd1OSJ9.qi3EiIQfhP7fegyhA9l9EA"
  });

  const zoom = [13.5];
  const [selectedCenterCoordinate, setSelectedCenterCoordinate] = useState([106.918963, 47.915473]);

  const centerCoordinates = {
    office: [106.918963, 47.915473],
    manifacture: [106.762418, 47.861269]
  }
  
  const placeCoordinates = {
    office: [106.9212113, 47.913584],
    manifacture: [106.762069, 47.859175]
  }

  return (
    <Map
      style="mapbox://styles/mapbox/streets-v8"
      zoom={zoom}
      center={selectedCenterCoordinate}
      on
      containerStyle={{
        height: "100%",
        width: "100%"
      }}>
        <Marker
          coordinates={placeCoordinates.office}
        >
          <div style={{display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column"}}>
            <div style={{fontSize: 13, fontWeight: "bold"}}>"Битумина Монгол ХХК" Төв оффис</div>
            <img alt="bitumina" height={30} width={30} src="/assets/img/logo-min.png"></img>
          </div>
        </Marker>
        <Marker
          coordinates={placeCoordinates.manifacture}
        >
          <div style={{display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column"}}>
            <div style={{fontSize: 13, fontWeight: "bold"}}>"Битумина Монгол ХХК" Үйлдвэр</div>
            <img alt="bitumina" height={30} width={30} src="/assets/img/logo-min.png"></img>
          </div>
        </Marker>
      </Map>
    )  
  }
