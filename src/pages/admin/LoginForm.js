import React, { useState } from "react";
import {
  CardContent,
  Card,
  TextField,
  Typography,
  Button,
  Grid,
  FormHelperText,
  Link,
} from "@material-ui/core";
import "./loginform.scss";
import { withRouter } from "react-router-dom";
import firebaseService from "../../service/FirebaseService";
import Notification from "../../components/notification/Notification";

const LoginForm = (props) => {
  const [username, setUsername] = useState("");
  const [usernameError, setUsernameError] = useState(false);
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [loginError, setLoginError] = useState("");

  const [notifIsOpen, setNotifIsOpen] = useState(false);
  const [notifStatus, setNotifStatus] = useState("success");
  const [notifDetail, setNotifDetail] = useState("");

  const login = () => {
    setLoginError(false)
    const usernameErr = checkNameError(username);
    const passwordErr = checkPassError(password);
    if (!usernameErr && !passwordErr) {
      firebaseService.signIn(username, password).then(userCredential => {
        props.history.push("/admin");
      }).catch((error) => {
        const { code, message } = error;
        setLoginError(`${code}: ${message}`);
      })
    }
  };

  const checkNameError = (name) => {
    if (!name || name.length < 4) {
      setUsernameError(true);
      return true;
    }
    setUsernameError(false);
    return false;
  };

  const checkPassError = (pass) => {
    if (!pass) {
      setPasswordError(true);
      return true;
    }
    setPasswordError(false);
    return false;
  };

  const changeUsername = (value) => {
    setUsername(value);
    setUsernameError(false);
  };

  const changePassword = (value) => {
    setPassword(value);
    setPasswordError(false);
  };

  const errorLogin = () => {
    if (!loginError) return null;
    return (
      <FormHelperText className="login-error" error={loginError ? true : false}>
        {loginError}
      </FormHelperText>
    );
  };

  const notifClose = () => {
    setNotifIsOpen(false);
  }

  const forgotPassword = () => {
    const usernameErr = checkNameError(username);
    if(!usernameErr) {
      firebaseService.auth.sendPasswordResetEmail(username).then(function() {
        setNotifStatus("success");
        setNotifDetail("Таны цахим шууданруу нууц үг сэргээх холбоос илгээлээ")
        setNotifIsOpen(true);
      }).catch((error) => {
        const { code, message } = error;
        setNotifStatus("success");
        setNotifDetail(`${code}:${message}`)
        setNotifIsOpen(true);
      })
    }
  }

  return (
    <div className="login-bg">
      <Card className="login-panel">
        <Typography variant="h4">Login form</Typography>
        <CardContent>
          <div className="login-input-group">
            <Grid item xs={12}>
              <TextField
                id="username"
                label="Email"
                required
                error={usernameError}
                className="login-input"
                onChange={(e) => changeUsername(e.target.value)}
                type="text"
                helperText={usernameError ? "Invalid email." : ""}
                size="small"
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                className="login-input"
                id="password"
                required
                error={passwordError}
                onChange={(e) => changePassword(e.target.value)}
                label="Password"
                size="small"
                helperText={passwordError ? "Invalid password." : ""}
                type="password"
                variant="outlined"
              />
              <Grid item xs={12}>
                  {errorLogin()}
              </Grid>
            </Grid>
            <Link
              component="button"
              variant="body2"
              onClick={() => forgotPassword()}
            >
              Forgot password?
            </Link>
            <Notification isOpen={notifIsOpen} close={notifClose} status={notifStatus} detail={notifDetail}/>
          </div>
          <Button
            variant="contained"
            color="primary"
            size="medium"
            className="login-btn"
            onClick={login}
          >
            Login
          </Button>
        </CardContent>
      </Card>
    </div>
  );
}
export default withRouter(LoginForm);