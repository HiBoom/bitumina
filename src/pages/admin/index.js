import React, { Suspense, useState, useEffect } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import SidebarContainer from "../../components/sidebar/SidebarContainer";
import "./style.scss";
import CircularLoader from "../../common/CircularLoader";
import FirebaseService from "../../service/FirebaseService";
const PostContainer = React.lazy(() =>
  import("../../components/post/PostContainer")
);
const ProductContainer = React.lazy(() =>
  import("../../components/product/ProductContainer")
);
const ServiceContainer = React.lazy(() =>
  import("../../components/service/ServiceContainer")
);
const PostForm = React.lazy(() => import("../../components/post/PostForm"));
const SignUpForm = React.lazy(() => import("../../components/sign-up-form/SignUpForm"));
const ProductForm = React.lazy(() => import("../../components/product/ProductForm"));
const ServiceForm = React.lazy(() => import("../../components/service/ServiceForm"));
const BannerForm = React.lazy(() => import("../../components/banner/BannerForm"));

const Admin = (props) => {
  const { match } = props;
  const [loggedIn, setLoggedIn] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    FirebaseService.auth.onAuthStateChanged((user) => {
      if(user) {
        setLoggedIn(true);
        setLoading(false);
      } else {
        setLoggedIn(false)
        setLoading(false);
      }
    })
    return () => null;
  }, []);

  const renderPrivateUrls = (isLogged) => {
    if (isLogged) {
      return (<>
        <Route
          path={`${match.url}/postList`}
          render={(props) => <PostContainer {...props} />}
        />
        <Route
          path={`${match.url}/productList`}
          render={(props) => <ProductContainer {...props} />}
        />
        <Route
          path={`${match.url}/createPost`}
          render={(props) => <PostForm {...props} />}
        />
        <Route
          path={`${match.url}/serviceList`}
          render={(props) => <ServiceContainer {...props} />}
        />
        <Route
          path={`${match.url}/createService`}
          render={(props) => <ServiceForm {...props} />}
        />
        <Route
          path={`${match.url}/contact`}
          render={(props) => <PostContainer {...props} />}
        />
        <Route
          path={`${match.url}/addUser`}
          render={(props) => <SignUpForm {...props} />}
        />
        <Route
          path={`${match.url}/addProduct`}
          render={(props) => <ProductForm {...props} />}
        />
        <Route
          path={`${match.url}/bannerForm`}
          render={(props) => <BannerForm {...props} />}
        />
      </>);
    }
    return null;
  };

  if(loading)
    return <CircularLoader />
  return (
    <SidebarContainer>
      <Suspense fallback={<div></div>}>
        <Switch>
          <Redirect
            exact
            from={`${match.url}/`}
            to={loggedIn ? `${match.url}/postList` : `/login`}
          />
          {renderPrivateUrls(loggedIn)}
          <Redirect to={match.url.startsWith("/admin") && !loggedIn ? "/login" : "/error"} />
        </Switch>
      </Suspense>
    </SidebarContainer>
  );
};
export default Admin;
