import React, { Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Header from "./layout/header";
import CircularLoader from "../common/CircularLoader";

const HomePage = React.lazy(() => import("./HomePage"));
const AboutPage = React.lazy(() => import("./AboutPage"));
const ContactPage = React.lazy(() => import("./ContactPage"));
const ServicesPage = React.lazy(() => import("./ServicesPage"));
const ServicePage = React.lazy(() => import("./ServicePage"));
const ProductsPage = React.lazy(() => import("./ProductsPage"));
const NewsPage = React.lazy(() => import("./NewsPage"));
const ProductPage = React.lazy(() => import("./ProductPage"));

const HomeIndex = (props) => {
  const { match } = props;

  return (
    <>
      <Header></Header>
      <Suspense fallback={<CircularLoader />}>
        <Switch>
          <Redirect exact from={`${match.url}/`} to={`${match.url}/home`} />
          <Route
            path={`${match.url}/home`}
            render={(props) => <HomePage {...props} />}
          />
          <Route path={`${match.url}/about`} render={(props) => <AboutPage {...props} />} />
          <Route
            path={`${match.url}/contact`}
            render={(props) => <ContactPage {...props} />}
          />
          <Route
            path={`${match.url}/services`}
            render={(props) => <ServicesPage {...props} />}
          />
          <Route
            path={`${match.url}/products`}
            render={(props) => <ProductsPage {...props} />}
          />
          <Route
            path={`${match.url}/news/:id`}
            render={(props) => <NewsPage {...props} />}
          />
          <Route
            path={`${match.url}/product/:id`}
            render={(props) => <ProductPage {...props} />}
          />
          <Route
            path={`${match.url}/service/:id`}
            render={(props) => <ServicePage {...props} />}
          />
          <Redirect to="/" />
        </Switch>
      </Suspense>
    </>
  );
};
export default HomeIndex;
